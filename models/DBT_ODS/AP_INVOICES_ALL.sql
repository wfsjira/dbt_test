--PROD1.AP.AP_INVOICES_ALL
--DBT can connect to fivetran, streamed tables will transformed in dbt first and then snowflake 
{{ config(
materialized='table'
) }}


select * from {{ source('DTP_RAW_PROD1_AP','AP_INVOICES_ALL')}}