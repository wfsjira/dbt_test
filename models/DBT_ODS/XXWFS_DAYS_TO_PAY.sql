--WFSAPPS.XXWFS_DAYS_TO_PAY
--DBT can connect to fivetran, streamed tables will transformed in dbt first and then snowflake 
{{ config(
materialized='table'
) }}


--select * from ods.XXWFS_DAYS_TO_PAY
select * from {{ source('DTP_RAW_ODS','XXWFS_DAYS_TO_PAY')}}