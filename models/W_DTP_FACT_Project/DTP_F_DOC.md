{% docs DBT_F_ORGANIZATION_ID %}
	
One of the following values: 

| status         | definition                                       |
|----------------|--------------------------------------------------|
| 129            | WFS Marine Operating Unit                        |
| 130            | WFS PAFCO Operating Unit                         |
| 126            | WFS Aviation Operating Unit                      |
| 128            | WFS Land Operating Unit                          |


{% enddocs %}