{{ config(
materialized='view'
) }}


SELECT 
F.LIFT_DATE				
,F.INVOICE_DATE			
,F.INV_HEADER_UPDATE_DATE	  
,F.INV_HEADER_CREATION_DATE	
,F.DATE_AVG				
,F.DUE_DATE				
,F.PAYMENT_DATE			
,F.DTP_ON_LIFT_DATE		
,F.DTP_ON_DUE_DATE		
,F.DTP_ON_INVOICE_DATE	
,F.WAVG_ON_INV_DATE		
,F.WAVG_ON_DUE_DATE		
,F.WAVG_ON_LIFT_DATE			
,F.ORIGINAL_AMOUNT		
,F.APPLIED_AMOUNT			
,F.AMOUNT_DUE_REMAINING_USD	
,F.WAVG					
,F.DTP					
,F.AMOUNT_APPLIED_USD		
,F.AMOUNT_DUE_REMAINING	
,F.ORIGINAL_USD_AMOUNT	
,F.CURRENCY_CODE	
,F.INVOICE_TYPE	
,F.VOUCHER_NUMBER	
,F.INVOICE_NUMBER	
,F.TERMS			
,F.POI			
,F.INVOICE_ID		
,F.VENDOR_ID     
,F.VENDOR_SITE_ID  
,F.ACCTS_PAY_CODE_COMBINATION_ID 
,F.CREATED_BY  
,F.LAST_UPDATED_BY  
,F.ORGANIZATION_ID 
,F.FUEL_FLAG      
,F.SOURCE_SYSTEM	
,F.INTEGRATION_ID
,SUPP.SUPPLIER_NAME
,SUPP.SUPPLIER_NUMBER
,SITE.SUPPLIER_SITE_CODE
,SITE.ADDRESS_LINE1
,SITE.ADDRESS_LINE2
,SITE.ADDRESS_LINE3
,SITE.ADDRESS_LINE4
,SITE.CITY
,SITE.STATE
,SITE.ZIP
,CREATED_BY.USER_NAME AS CREATED_BY_USER_NAME
,CREATED_BY.USER_FULL_NAME AS CREATED_BY_USER_FULL_NAME
,UPDATED_BY.USER_NAME AS UPDATED_BY_USER_NAME
,UPDATED_BY.USER_FULL_NAME AS UPDATED_BY_USER_FULL_NAME
,GL.COMPANY_CODE
,GL.COMPANY_DESC
,GL.OFFICE_CODE
,GL.OFFICE_DESC
,GL.DEPARTMENT_CODE
,GL.DEPARTMENT_DESC
,GL.ACCOUNT_CODE
,GL.ACCOUNT_DESC
,GL.EMPLOYEE_CODE
,GL.EMPLOYEE_DESC
,ORG.ORG_NAME
,ORG.ORG_ID
FROM 
{{ref('W_DTP_F')}} AS F,
BIDW.W_SUPPLIER_D AS SUPP,
BIDW.W_SUPPLIER_SITE_D AS SITE,
BIDW.W_USER_D AS CREATED_BY,
BIDW.W_USER_D AS UPDATED_BY,
BIDW.W_GL_ACCOUNT_D  AS GL,
{{ref('W_ORG_D')}} AS ORG
WHERE
F.SUPPLIER_SK = SUPP.SUPPLIER_SK
AND F.SUPPLIER_SITE_SK = SITE.SUPPLIER_SITE_SK
AND F.INV_HEADER_CREATED_BY_SK = CREATED_BY.USER_SK
AND F.INV_HEADER_UPDATED_BY_SK = UPDATED_BY.USER_SK
AND F.GL_CODE_SK = GL.GL_CODE_SK
AND F.ORGANIZATION_SK = ORG.ORG_SK