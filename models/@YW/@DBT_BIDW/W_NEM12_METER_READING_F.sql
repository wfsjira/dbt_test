--case when FS.NMI is null then 0 else NVL(SITE.NMI_LME_SITE_SK, -1) end AS NMI_LME_SITE_SK 
{{ config(materialized='incremental', unique_key='INTEGRATION_ID') }}

SELECT FS.*, case when FS.NMI is null then '0' else NVL(SITE.NMI_LME_SITE_SK, '-1') END AS NMI_LME_SITE_SK
FROM {{ref('W_NEM12_METER_READING_FS')}} FS
LEFT JOIN {{ref('W_NMI_LME_SITE_D')}} SITE ON FS.NMI || '~' || 'KINECT AUSTRALIA' = SITE.INTEGRATION_ID

{% if is_incremental() %}

  -- this filter will only be applied on an incremental run
  where FS.INTEGRATION_ID NOT IN (select INTEGRATION_ID from {{ this }})

{% endif %}