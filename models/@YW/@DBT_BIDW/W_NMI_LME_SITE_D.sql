{{config(materialized = 'table')}}

SELECT * ,{{ dbt_utils.surrogate_key('NMI') }} as NMI_LME_SITE_SK
FROM {{ref('W_NMI_LME_SITE_DS')}}