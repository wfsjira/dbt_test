{{config(materialized = 'view')}}
WITH SubQuery AS (
WITH Holiday AS (
		SELECT DATEADD('DAY',-2,date(date)) PRICE_DATE, date(date) DATE, date(date) DATE2, 'Optional' AS holiday, 'Optional' AS holiday2 FROM {{ref('HOLIDAY')}}		
		UNION
		SELECT DATEADD('DAY',-1,date(date)) PRICE_DATE, date(date) DATE, date(date) DATE2, holiday, 'Real Holiday' as holiday2 FROM {{ref('HOLIDAY')}}		
		UNION	
		SELECT date(date) PRICE_DATE, date(date) DATE, date(date) DATE2, holiday, 'Real Holiday' as holiday2 FROM {{ref('HOLIDAY')}}
		UNION
		SELECT DATEADD('DAY',1,date(date)) PRICE_DATE, date(date) DATE, NULL AS DATE2, holiday, NULL AS holiday2 FROM {{ref('HOLIDAY')}}
					)
	SELECT DTNP.*, 
		DAYNAME(date(to_char(DTNP.PRICE_DATE),'YYYYMMDD')) AS Datename, 
		CASE WHEN DAYNAME(date(to_char(DTNP.PRICE_DATE),'YYYYMMDD')) = 'Sun' THEN dateadd(DAY, -1, date(to_char(DTNP.PRICE_DATE))) 
			 --WHEN DAYNAME(date(PRICE_DATE, 'YYYYMMDD')) = 'Sat' THEN dateadd(DAY, -1, date(PRICE_DATE, 'YYYYMMDD'))
			 ELSE date(to_char(DTNP.PRICE_DATE),'YYYYMMDD')
		END AS Every_Week,
		CASE WHEN DAYNAME(date(to_char(DTNP.PRICE_DATE),'YYYYMMDD')) IN ('Sat','Sun') AND COUNT(DTNP.price_date) OVER (PARTITION BY market, prod_abbr_6, supplier, brand_type, TCN_CODE, Every_Week)=2 THEN 'NewP'
			 --WHEN DAYNAME(date(PRICE_DATE, 'YYYYMMDD')) IN ( 'Fri', 'Sat','Sun') AND COUNT(price_date) OVER (PARTITION BY market, prod_abbr_6, supplier, brand_type, TCN_CODE, Every_Week)=3 THEN 'NewP2'
			 ELSE NULL END AS NewPrice,
		Holiday.Date,
		Holiday.Date2,
		Holiday.Holiday,
		Holiday.Holiday2,
		CASE WHEN holiday.holiday IS NOT NULL AND COUNT(DTNP.price_date) OVER (PARTITION BY market, prod_abbr_6, supplier, brand_type, TCN_CODE, holiday.date)=2 AND date(to_char(min(DTNP.price_date) OVER (PARTITION BY market, prod_abbr_6, supplier, brand_type, TCN_CODE, holiday.date)),'YYYYMMDD') = DATEADD('DAY',-2,date) THEN 'NewPH1'
			 WHEN holiday.holiday IS NOT NULL AND COUNT(DTNP.price_date) OVER (PARTITION BY market, prod_abbr_6, supplier, brand_type, TCN_CODE, holiday.date)=2 AND date(to_char(min(DTNP.price_date) OVER (PARTITION BY market, prod_abbr_6, supplier, brand_type, TCN_CODE, holiday.date)),'YYYYMMDD') = DATEADD('DAY',-1,date) THEN 'NewPH2'
			 WHEN holiday.holiday IS NOT NULL AND COUNT(DTNP.price_date) OVER (PARTITION BY market, prod_abbr_6, supplier, brand_type, TCN_CODE, holiday.date)=2 AND date(to_char(min(DTNP.price_date) OVER (PARTITION BY market, prod_abbr_6, supplier, brand_type, TCN_CODE, holiday.date)),'YYYYMMDD') = date THEN 'NewPH3'
			 WHEN holiday.holiday IS NOT NULL AND COUNT(DTNP.price_date) OVER (PARTITION BY market, prod_abbr_6, supplier, brand_type, TCN_CODE, holiday.date)=3 AND date(to_char(max(DTNP.price_date) OVER (PARTITION BY market, prod_abbr_6, supplier, brand_type, TCN_CODE, holiday.date)),'YYYYMMDD') = date THEN 'NewPH4'		 
			 WHEN holiday.holiday IS NOT NULL AND COUNT(DTNP.price_date) OVER (PARTITION BY market, prod_abbr_6, supplier, brand_type, TCN_CODE, holiday.date) IN (3,4) AND date(to_char(max(DTNP.price_date) OVER (PARTITION BY market, prod_abbr_6, supplier, brand_type, TCN_CODE, holiday.date)),'YYYYMMDD') = DATEADD('DAY',1,date) THEN 'NewPH5'
			 ELSE NULL END AS NewPriceH,
		CASE WHEN holiday.holiday2 IS NOT NULL AND COUNT(DTNP.price_date) OVER (PARTITION BY market, prod_abbr_6, supplier, brand_type, TCN_CODE, holiday.date2)=2 AND date(to_char(max(DTNP.price_date) OVER (PARTITION BY market, prod_abbr_6, supplier, brand_type, TCN_CODE, holiday.date2)),'YYYYMMDD') = date THEN 'NewPH2' 
			 WHEN holiday.holiday2 IS NOT NULL AND COUNT(DTNP.price_date) OVER (PARTITION BY market, prod_abbr_6, supplier, brand_type, TCN_CODE, holiday.date2)=2 AND date(to_char(max(DTNP.price_date) OVER (PARTITION BY market, prod_abbr_6, supplier, brand_type, TCN_CODE, holiday.date2)),'YYYYMMDD') = DATEADD('DAY',-1,date) THEN 'NewPH1' 
			 WHEN holiday.holiday2 IS NOT NULL AND COUNT(DTNP.price_date) OVER (PARTITION BY market, prod_abbr_6, supplier, brand_type, TCN_CODE, holiday.date2)=3 THEN 'NewPH3'
			 ELSE NULL END AS NewPriceH2
			 
	FROM {{ref('DTN_PRICES_V')}} DTNP
	LEFT JOIN Holiday ON date(TO_CHAR(DTNP.PRICE_DATE),'YYYYMMDD') = Holiday.price_date
	),
	
 G_AGG AS (
 	---Price for Sunday
	SELECT PROD_ABBR_6, every_week, MARKET, 
					avg(price_gross) AS RA, 
					min(price_gross) AS RL, 
					max(price_gross) AS RH, 
					AVG(CASE WHEN brand_type = 'B' THEN PRICE_GROSS END) AS BA,
			   		AVG(CASE WHEN brand_type = 'U' THEN PRICE_GROSS END) AS UA,
			   		MAX(CASE WHEN brand_type = 'B' THEN PRICE_GROSS END) AS BH,
			  	 	MAX(CASE WHEN brand_type = 'U' THEN PRICE_GROSS END) AS UH,
			   		min(CASE WHEN brand_type = 'B' THEN PRICE_GROSS END) AS BL,
			   		min(CASE WHEN brand_type = 'U' THEN PRICE_GROSS END) AS UL,
					MAX(PRICE_DATE_LOGIC) AS PDL1, 
					max(date) AS Holiday_Date,
					dateadd(DAY,+1, every_week) AS PRICE_DATE,
					max(Holiday2) AS Holiday2
				FROM SubQuery
				WHERE ((Datename = 'Sat' AND NewPrice IS NULL) OR (Datename = 'Sun'))
					AND PROD_ABBR_6 IS NOT NULL
					AND every_week IS NOT NULL
					AND MARKET IS NOT NULL
					AND brand_type IS NOT NULL
				GROUP BY PROD_ABBR_6, every_week, MARKET
				HAVING Holiday_Date != every_week
					AND Holiday_Date != dateadd(DAY,+1, every_week)
					OR holiday_date IS NULL
				
				UNION ALL
				
	---Price for Monday			
	SELECT PROD_ABBR_6, every_week, MARKET, 
					avg(price_gross) AS RA, 
					min(price_gross) AS RL, 
					max(price_gross) AS RH, 
					AVG(CASE WHEN brand_type = 'B' THEN PRICE_GROSS END) AS BA,
			   		AVG(CASE WHEN brand_type = 'U' THEN PRICE_GROSS END) AS UA,
			   		MAX(CASE WHEN brand_type = 'B' THEN PRICE_GROSS END) AS BH,
			  	 	MAX(CASE WHEN brand_type = 'U' THEN PRICE_GROSS END) AS UH,
			   		min(CASE WHEN brand_type = 'B' THEN PRICE_GROSS END) AS BL,
			   		min(CASE WHEN brand_type = 'U' THEN PRICE_GROSS END) AS UL,
					MAX(PRICE_DATE_LOGIC) AS PDL1, 
					max(date) AS Holiday_Date,
					dateadd(DAY,+2, every_week) AS PRICE_DATE,
					max(Holiday2) AS Holiday2
				FROM SubQuery
				WHERE ((Datename = 'Sat' AND NewPrice IS NULL) OR (Datename = 'Sun'))
					AND PROD_ABBR_6 IS NOT NULL
					AND every_week IS NOT NULL
					AND MARKET IS NOT NULL
					AND brand_type IS NOT NULL
				GROUP BY PROD_ABBR_6, every_week, MARKET
				HAVING Holiday_Date != DATEADD('DAY',+1,every_week)
					AND Holiday_Date != DATEADD('DAY',+2,every_week)
					OR holiday_date IS NULL

				
			--	SELECT PROD_ABBR_6, every_week, MARKET, 
				--	avg(price_gross) AS RA, 
				--	min(price_gross) AS RL, 
				--	max(price_gross) AS RH,
				--	AVG(CASE WHEN brand_type = 'B' THEN PRICE_GROSS END) AS BA,
			 --  	AVG(CASE WHEN brand_type = 'U' THEN PRICE_GROSS END) AS UA,
			  -- 	MAX(CASE WHEN brand_type = 'B' THEN PRICE_GROSS END) AS BH,
			  --	MAX(CASE WHEN brand_type = 'U' THEN PRICE_GROSS END) AS UH,
			  -- 	min(CASE WHEN brand_type = 'B' THEN PRICE_GROSS END) AS BL,
			  -- 	min(CASE WHEN brand_type = 'U' THEN PRICE_GROSS END) AS UL,
				--	MAX(PRICE_DATE_LOGIC) AS PDL1, 
				--	dateadd(DAY,+2, every_week) AS PRICE_DATE
			--	FROM SubQuery
			--	WHERE (Datename IN ('Fri', 'Sat') AND NewPrice IS NULL) OR (Datename = 'Sat' AND NewPrice = 'NewP') OR (Datename = 'Sun')
				--	AND PROD_ABBR_6 IS NOT NULL
				--	AND every_week IS NOT NULL
				--	AND MARKET IS NOT NULL
				--	AND brand_type IS NOT NULL
				--	AND (suffix != '@' OR suffix IS NULL)
				--GROUP BY PROD_ABBR_6, every_week, MARKET
				
				UNION ALL
				
				---Price for Tuesday to Saturday							
				SELECT PROD_ABBR_6, every_week, MARKET, 
					avg(price_gross) AS RA, 
					min(price_gross) AS RL, 
					max(price_gross) AS RH, 
					AVG(CASE WHEN brand_type = 'B' THEN PRICE_GROSS END) AS BA,
			   		AVG(CASE WHEN brand_type = 'U' THEN PRICE_GROSS END) AS UA,
			   		MAX(CASE WHEN brand_type = 'B' THEN PRICE_GROSS END) AS BH,
			  	 	MAX(CASE WHEN brand_type = 'U' THEN PRICE_GROSS END) AS UH,
			   		min(CASE WHEN brand_type = 'B' THEN PRICE_GROSS END) AS BL,
			   		min(CASE WHEN brand_type = 'U' THEN PRICE_GROSS END) AS UL,
					MAX(PRICE_DATE_LOGIC) AS PDL1,
					max(date) AS Holiday_Date,
					every_week AS PRICE_DATE,
					max(Holiday2) AS Holiday2
				FROM SubQuery
				WHERE Datename IN ('Tue', 'Wed', 'Thu', 'Fri', 'Sat')
					AND PROD_ABBR_6 IS NOT NULL
					AND every_week IS NOT NULL
					AND MARKET IS NOT NULL
					AND brand_type IS NOT NULL			 
				GROUP BY PROD_ABBR_6, every_week, MARKET
				HAVING Holiday_Date != DATEADD('DAY',-1,every_week)
					AND Holiday_Date != every_week
					OR holiday_date IS NULL
				
				UNION ALL
				
				----Price for the day after Holiday - Not Monday
				SELECT PROD_ABBR_6, date, MARKET, 
					avg(price_gross) AS RA, 
					min(price_gross) AS RL, 
					max(price_gross) AS RH, 
					AVG(CASE WHEN brand_type = 'B' THEN PRICE_GROSS END) AS BA,
			   		AVG(CASE WHEN brand_type = 'U' THEN PRICE_GROSS END) AS UA,
			   		MAX(CASE WHEN brand_type = 'B' THEN PRICE_GROSS END) AS BH,
			  	 	MAX(CASE WHEN brand_type = 'U' THEN PRICE_GROSS END) AS UH,
			   		min(CASE WHEN brand_type = 'B' THEN PRICE_GROSS END) AS BL,
			   		min(CASE WHEN brand_type = 'U' THEN PRICE_GROSS END) AS UL,
					MAX(PRICE_DATE_LOGIC) AS PDL1, 
					max(date) AS Holiday_Date,
					dateadd('day', 1, date) AS PRICE_DATE,
					max(Holiday2) AS Holiday2
				FROM SubQuery
				WHERE
					(	(NewPriceH IS NULL) OR
						(NewPriceH = 'NewPH1') OR 
						(date(to_char(PRICE_DATE),'YYYYMMDD') = date AND NewPriceH in('NewPH4', 'NewPH2')) OR
						(date(to_char(PRICE_DATE),'YYYYMMDD') = dateadd('day',1, date) AND NewPriceH IN ('NewPH2', 'NewPH3','NewPH5'))
					)
					AND PROD_ABBR_6 IS NOT NULL
					AND date IS NOT NULL
					AND Holiday != 'Optional'
					AND MARKET IS NOT NULL
					AND brand_type IS NOT NULL
				GROUP BY PROD_ABBR_6, date, MARKET
				HAVING dayname(Holiday_Date)!='Mon'
				
				UNION ALL
				----Price for the day after Holiday - Monday
				SELECT PROD_ABBR_6, date, MARKET, 
					avg(price_gross) AS RA, 
					min(price_gross) AS RL, 
					max(price_gross) AS RH, 
					AVG(CASE WHEN brand_type = 'B' THEN PRICE_GROSS END) AS BA,
			   		AVG(CASE WHEN brand_type = 'U' THEN PRICE_GROSS END) AS UA,
			   		MAX(CASE WHEN brand_type = 'B' THEN PRICE_GROSS END) AS BH,
			  	 	MAX(CASE WHEN brand_type = 'U' THEN PRICE_GROSS END) AS UH,
			   		min(CASE WHEN brand_type = 'B' THEN PRICE_GROSS END) AS BL,
			   		min(CASE WHEN brand_type = 'U' THEN PRICE_GROSS END) AS UL,
					MAX(PRICE_DATE_LOGIC) AS PDL1, 
					max(date) AS Holiday_Date,
					dateadd('day', 1, date) AS PRICE_DATE,
					max(Holiday2) AS Holiday2
				FROM SubQuery
				WHERE
					(	(NewPriceH IS NULL) OR
						(date(to_char(PRICE_DATE),'YYYYMMDD') = dateadd('day',-1, date) AND NewPriceH IN ('NewPH1')) OR 
						(date(to_char(PRICE_DATE),'YYYYMMDD') = date AND NewPriceH IN ('NewPH1', 'NewPH2', 'NewPH4')) OR
						(date(to_char(PRICE_DATE),'YYYYMMDD') = dateadd('day',1, date) AND NewPriceH IN ('NewPH1','NewPH2','NewPH3', 'NewPH5'))
					)
					AND PROD_ABBR_6 IS NOT NULL
					AND date IS NOT NULL
					AND MARKET IS NOT NULL
					AND brand_type IS NOT NULL
				GROUP BY PROD_ABBR_6, date, MARKET
				HAVING dayname(Holiday_Date)='Mon'
				
				UNION ALL
				
			----Price on Holiday - Not Monday
				SELECT PROD_ABBR_6, date2 AS date, MARKET, 
					avg(price_gross) AS RA, 
					min(price_gross) AS RL, 
					max(price_gross) AS RH, 
					AVG(CASE WHEN brand_type = 'B' THEN PRICE_GROSS END) AS BA,
			   		AVG(CASE WHEN brand_type = 'U' THEN PRICE_GROSS END) AS UA,
			   		MAX(CASE WHEN brand_type = 'B' THEN PRICE_GROSS END) AS BH,
			  	 	MAX(CASE WHEN brand_type = 'U' THEN PRICE_GROSS END) AS UH,
			   		min(CASE WHEN brand_type = 'B' THEN PRICE_GROSS END) AS BL,
			   		min(CASE WHEN brand_type = 'U' THEN PRICE_GROSS END) AS UL,
					MAX(PRICE_DATE_LOGIC) AS PDL1, 
					max(date2) AS Holiday_Date,
					date2 AS PRICE_DATE,
					max(Holiday2) AS Holiday2
				FROM SubQuery
				WHERE
					(	(NewPriceH2 IS NULL) OR
						(date(to_char(PRICE_DATE)) = date2 ) OR
					    (NewPriceH2 IN ('NewPH1'))
					)
					AND PROD_ABBR_6 IS NOT NULL
					AND Holiday2 IS NOT NULL
					AND Holiday2 != 'Optional'
					AND MARKET IS NOT NULL
					AND brand_type IS NOT NULL
				GROUP BY PROD_ABBR_6, date2, MARKET
				HAVING dayname(Holiday_Date)!='Mon'
				
				UNION ALL
				
				----Price for Holiday - Monday
				SELECT PROD_ABBR_6, date2 AS date, MARKET, 
					avg(price_gross) AS RA, 
					min(price_gross) AS RL, 
					max(price_gross) AS RH, 
					AVG(CASE WHEN brand_type = 'B' THEN PRICE_GROSS END) AS BA,
			   		AVG(CASE WHEN brand_type = 'U' THEN PRICE_GROSS END) AS UA,
			   		MAX(CASE WHEN brand_type = 'B' THEN PRICE_GROSS END) AS BH,
			  	 	MAX(CASE WHEN brand_type = 'U' THEN PRICE_GROSS END) AS UH,
			   		min(CASE WHEN brand_type = 'B' THEN PRICE_GROSS END) AS BL,
			   		min(CASE WHEN brand_type = 'U' THEN PRICE_GROSS END) AS UL,
					MAX(PRICE_DATE_LOGIC) AS PDL1, 
					max(date2) AS Holiday_Date,
					date2 AS PRICE_DATE,
					max(Holiday2) AS Holiday2
				FROM SubQuery
				WHERE
					(	(NewPriceH2 IS NULL) OR
						(date(to_char(PRICE_DATE),'YYYYMMDD') = date2 AND NewPriceH2 in('NewPH3', 'NewPH2')) OR
						(date(to_char(PRICE_DATE),'YYYYMMDD') = dateadd('day',-1, date2) AND NewPriceH2 IN ('NewPH1'))
					)
					AND PROD_ABBR_6 IS NOT NULL
					AND Holiday2 IS NOT NULL
					AND MARKET IS NOT NULL
					AND brand_type IS NOT NULL
				GROUP BY PROD_ABBR_6, date2, MARKET
				HAVING dayname(Holiday_Date)='Mon'
						),
					
		  	DT AS (
				SELECT 
					TCN, 
					MAX(CITY_STATE) AS Terminal_State, 
					MAX(TERMINAL_ADDRESS) AS Terminal_Address
				FROM BIDW.DTN_TERMINALS
				GROUP BY TCN
				  )
				  
SELECT 
	D1.PRICE_DATE AS PRICE_DATE_SK,
	date(to_char(D1.PRICE_DATE),'YYYYMMDD') AS PRICE_DATE,
	D1.FILE_DT_TIME AS FILE_DT_TIME,
	CAST (D1.PRICE_DATE_LOGIC AS varchar(255)) AS PRICE_DATE_LOGIC,
	CAST (D1.MARKET AS varchar(255)) AS MARKET,
	CAST (D1.SUPPLIER AS varchar(255)) AS SUPPLIER,
	CAST (D1.BRAND_TYPE AS varchar(255)) AS BRAND_TYPE,
	CAST (D1.TERMINAL_OWNER_CODE AS varchar(255)) AS TERMINAL,
	CAST (D1.TCN_CODE AS varchar(255)) AS TCN,
	CAST (D1.PROD_ABBR_6 AS varchar(255)) AS PRODUCT_CODE,
	CAST (D1.PROD_NAME AS varchar(255)) AS PRODUCT_NAME,
	CAST (D1.LATITUDE AS varchar(255)) AS LATITUDE,
	CAST (D1.LONGITUDE AS varchar(255)) AS LONGITUDE,
	CAST (D1.PROD_GROUP_NAME AS varchar(255)) AS PRODUCT_GROUP,
	CAST (D1.GrossNet AS varchar(255)) AS GROSS_NET,
	CAST (D1."'Others'" AS decimal(38,6)) AS SUPPLIER_PRICE,
	CASE WHEN D1."'Others'" IS NULL THEN NULL ELSE D1.MOVE_AMT END AS MOVE,
	CAST(D1."'RA'" AS decimal(38,6))  AS RA,
	CAST(D1."'BA'" AS decimal(38,6))  AS BA,
	CAST(D1."'UA'" AS decimal(38,6))  AS UA,
	CAST(D1."'RH'" AS decimal(38,6))  AS RH,
	CAST(D1."'BH'" AS decimal(38,6))  AS BH,
	CAST(D1."'UH'" AS decimal(38,6))  AS UH,
	CAST(D1."'RL'" AS decimal(38,6))  AS RL,
	CAST(D1."'BL'" AS decimal(38,6))  AS BL,
	CAST(D1."'UL'" AS decimal(38,6))  AS UL,
	CAST(D1."'CA'" AS decimal(38,6))  AS CA,
	CAST(D1."'TA'" AS decimal(38,6))  AS TA,
	CAST (D1.PAYM_TERM AS varchar(255)) AS PAYMENT_TERMS,
	CAST (D1.SUPPLIER_NAME AS varchar(255)) AS SUPPLIER_NAME,
	CAST (D1.PRODUCT_CATEGORY AS varchar(255)) AS PRODUCT_CATEGORY,
	D1.EFFECTIVE_DT	AS EFFECTIVE_DT,
	D1.TERMINAL_ADDRESS AS TERMINAL_ADDRESS,
	CAST (D1.TERMINAL_CITY_NAME AS varchar(255)) AS TERMINAL_CITY_NAME,
	D1.TERMINAL_STATE AS TERMINAL_STATE,
	CAST (D1.TERMINAL_ZIP AS varchar(255)) AS TERMINAL_ZIP,
	CAST (D1.TERMINAL_CITY AS varchar(255)) AS TERMINAL_CITY,
	CAST (D1.SUFFIX AS varchar(255)) AS SUFFIX,
	CAST (D1.PROD_ALL_SEASON AS varchar(255)) AS PROD_ALL_SEASON
FROM (
	WITH A AS (
	SELECT 
			DP.PRICE_DATE, 
			DP.MARKET,
			max(DP.FILE_DT_TIME) OVER (PARTITION BY dp.PRICE_DATE, dp.market, ds.NAME, dp.PROD_ABBR_6, dp.BRAND_TYPE, dp.TCN_CODE) AS FILE_DT_TIME,
			DP.BRAND_TYPE,
			DP.MOVE_AMT,
			DP.EFFECTIVE_DT,
			CASE WHEN DP.CONFIRM_IND = 'Y' THEN DP.SUPPLIER ELSE NULL END AS Supplier,
			DP.TCN_CODE,
			DP.TERMINAL_CITY,
			DP.TERMINAL_OWNER_CODE,
			DP.PAYM_TERM,
			CAST(CASE WHEN DP.SUPPLIER = 'RA' THEN G_AGG.RA
					  WHEN DP.SUPPLIER = 'UA' THEN G_AGG.UA
					  WHEN DP.SUPPLIER = 'BA' THEN G_AGG.BA
					  WHEN DP.SUPPLIER = 'RH' THEN G_AGG.RH
					  WHEN DP.SUPPLIER = 'UH' THEN G_AGG.UH
					  WHEN DP.SUPPLIER = 'BH' THEN G_AGG.BH
					  WHEN DP.SUPPLIER = 'RL' THEN G_AGG.RL
					  WHEN DP.SUPPLIER = 'UL' THEN G_AGG.UL
					  WHEN DP.SUPPLIER = 'BL' THEN G_AGG.BL
				 	  ELSE DP.PRICE_GROSS END AS decimal(38,26)) AS Gross,
			CAST(DP.PRICE_NET AS decimal(38,26)) AS Net,
			DP.SUFFIX,
			DP.TERMINAL_CITY_NAME,
			DT.TERMINAL_STATE,
			DP.LATITUDE,
			DP.LONGITUDE,
			DT.TERMINAL_ADDRESS,
			DP.TERMINAL_ZIP,
			DP.PROD_NAME,
			DP.PROD_ABBR_6,
			DP.PROD_GROUP_NAME,
			DP.PRODUCT_CATEGORY,
			CASE WHEN DP.CONFIRM_IND IS NULL THEN DP.SUPPLIER ELSE 'Others' END AS Aggregate,
			DS.NAME AS Supplier_Name,
			CASE WHEN dp.SUPPLIER IN ('RA','BA','UA','RH','BH','UH','RL','BL','UL', 'TA', 'CA') AND (dp.PRICE_GROSS IS NULL AND dp.brand_type IS NULL) THEN dp.price_date_logic
			     WHEN dp.SUPPLIER IN ('RA','BA','UA','RH','BH','UH','RL','BL','UL', 'TA', 'CA') AND (dp.PRICE_net IS NULL AND dp.brand_type IS NULL) THEN G_AGG.PDL1
			ELSE DP.PRICE_DATE_LOGIC END AS PRICE_DATE_LOGIC,
			DP.Prod_all_season
		FROM SubQuery DP
		LEFT JOIN G_AGG ON DP.MARKET = G_AGG.MARKET AND date(to_char(dp.PRICE_DATE),'YYYYMMDD') = G_AGG.PRICE_DATE AND DP.PROD_ABBR_6 = G_AGG.PROD_ABBR_6
		LEFT JOIN DT ON DP.TCN_CODE = DT.TCN
		LEFT JOIN {{source('DTN_HELPERS','DTN_SUPPLIERS')}} DS ON DS.ABBR = DP.SUPPLIER AND DS.BRAND_TYPE = DP.BRAND_TYPE
		WHERE ((DP.CONFIRM_IND IS NULL and DP.SUPPLIER IN ('RA','BA','UA','RH','BH','UH','RL','BL','UL', 'TA', 'CA')) OR DP.CONFIRM_IND = 'Y')
		)	  
				  
SELECT * FROM A

	UNPIVOT
	(
		Value
		FOR GrossNet IN (GROSS, NET)
	)
	PIVOT
	(
		MIN(Value)
		FOR Aggregate IN ('RA', 'BA', 'UA', 'RH', 'BH', 'UH', 'RL', 'BL', 'UL', 'TA', 'CA', 'Others')
	)				  
) D1