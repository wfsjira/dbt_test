{{config(materialized = 'ephemeral')}}

SELECT DTNP.*, 
		DAYNAME(date(to_char(DTNP.PRICE_DATE),'YYYYMMDD')) AS Datename, 
		CASE WHEN DAYNAME(date(to_char(DTNP.PRICE_DATE),'YYYYMMDD')) = 'Sun' THEN dateadd(DAY, -1, date(to_char(DTNP.PRICE_DATE))) 
			 --WHEN DAYNAME(date(PRICE_DATE, 'YYYYMMDD')) = 'Sat' THEN dateadd(DAY, -1, date(PRICE_DATE, 'YYYYMMDD'))
			 ELSE date(to_char(DTNP.PRICE_DATE),'YYYYMMDD')
		END AS Every_Week,
		CASE WHEN DAYNAME(date(to_char(DTNP.PRICE_DATE),'YYYYMMDD')) IN ('Sat','Sun') AND COUNT(DTNP.price_date) OVER (PARTITION BY market, prod_abbr_6, supplier, brand_type, TCN_CODE, Every_Week)=2 THEN 'NewP'
			 --WHEN DAYNAME(date(PRICE_DATE, 'YYYYMMDD')) IN ( 'Fri', 'Sat','Sun') AND COUNT(price_date) OVER (PARTITION BY market, prod_abbr_6, supplier, brand_type, TCN_CODE, Every_Week)=3 THEN 'NewP2'
			 ELSE NULL END AS NewPrice,
		Holiday.Date,
		Holiday.Date2,
		Holiday.Holiday,
		Holiday.Holiday2,
		CASE WHEN holiday.holiday IS NOT NULL AND COUNT(DTNP.price_date) OVER (PARTITION BY market, prod_abbr_6, supplier, brand_type, TCN_CODE, holiday.date)=2 AND date(to_char(min(DTNP.price_date) OVER (PARTITION BY market, prod_abbr_6, supplier, brand_type, TCN_CODE, holiday.date)),'YYYYMMDD') = DATEADD('DAY',-2,date) THEN 'NewPH1'
			 WHEN holiday.holiday IS NOT NULL AND COUNT(DTNP.price_date) OVER (PARTITION BY market, prod_abbr_6, supplier, brand_type, TCN_CODE, holiday.date)=2 AND date(to_char(min(DTNP.price_date) OVER (PARTITION BY market, prod_abbr_6, supplier, brand_type, TCN_CODE, holiday.date)),'YYYYMMDD') = DATEADD('DAY',-1,date) THEN 'NewPH2'
			 WHEN holiday.holiday IS NOT NULL AND COUNT(DTNP.price_date) OVER (PARTITION BY market, prod_abbr_6, supplier, brand_type, TCN_CODE, holiday.date)=2 AND date(to_char(min(DTNP.price_date) OVER (PARTITION BY market, prod_abbr_6, supplier, brand_type, TCN_CODE, holiday.date)),'YYYYMMDD') = date THEN 'NewPH3'
			 WHEN holiday.holiday IS NOT NULL AND COUNT(DTNP.price_date) OVER (PARTITION BY market, prod_abbr_6, supplier, brand_type, TCN_CODE, holiday.date)=3 AND date(to_char(max(DTNP.price_date) OVER (PARTITION BY market, prod_abbr_6, supplier, brand_type, TCN_CODE, holiday.date)),'YYYYMMDD') = date THEN 'NewPH4'		 
			 WHEN holiday.holiday IS NOT NULL AND COUNT(DTNP.price_date) OVER (PARTITION BY market, prod_abbr_6, supplier, brand_type, TCN_CODE, holiday.date) IN (3,4) AND date(to_char(max(DTNP.price_date) OVER (PARTITION BY market, prod_abbr_6, supplier, brand_type, TCN_CODE, holiday.date)),'YYYYMMDD') = DATEADD('DAY',1,date) THEN 'NewPH5'
			 ELSE NULL END AS NewPriceH,
		CASE WHEN holiday.holiday2 IS NOT NULL AND COUNT(DTNP.price_date) OVER (PARTITION BY market, prod_abbr_6, supplier, brand_type, TCN_CODE, holiday.date2)=2 AND date(to_char(max(DTNP.price_date) OVER (PARTITION BY market, prod_abbr_6, supplier, brand_type, TCN_CODE, holiday.date2)),'YYYYMMDD') = date THEN 'NewPH2' 
			 WHEN holiday.holiday2 IS NOT NULL AND COUNT(DTNP.price_date) OVER (PARTITION BY market, prod_abbr_6, supplier, brand_type, TCN_CODE, holiday.date2)=2 AND date(to_char(max(DTNP.price_date) OVER (PARTITION BY market, prod_abbr_6, supplier, brand_type, TCN_CODE, holiday.date2)),'YYYYMMDD') = DATEADD('DAY',-1,date) THEN 'NewPH1' 
			 WHEN holiday.holiday2 IS NOT NULL AND COUNT(DTNP.price_date) OVER (PARTITION BY market, prod_abbr_6, supplier, brand_type, TCN_CODE, holiday.date2)=3 THEN 'NewPH3'
			 ELSE NULL END AS NewPriceH2
			 
	FROM {{ref('DTN_PRICES_V')}} DTNP
	LEFT JOIN {{ref('Holiday')}} Holiday ON date(TO_CHAR(DTNP.PRICE_DATE),'YYYYMMDD') = Holiday.price_date