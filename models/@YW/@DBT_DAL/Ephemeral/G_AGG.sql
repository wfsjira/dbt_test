{{config(materialized = 'ephemeral')}}

SELECT * FROM {{ref('Price_for_Monday')}}
UNION ALL
SELECT * FROM {{ref('Price_for_Sunday')}}
UNION ALL
SELECT * FROM {{ref('Price_for_Tue_to_Sat')}}