{{config(materialized = 'ephemeral')}}

SELECT 
    TCN, 
    MAX(CITY_STATE) AS Terminal_State, 
    MAX(TERMINAL_ADDRESS) AS Terminal_Address
FROM BIDW.DTN_TERMINALS
GROUP BY TCN