{{config(materialized = 'ephemeral')}}

SELECT 
        DP.PRICE_DATE, 
        DP.MARKET,
        max(DP.FILE_DT_TIME) OVER (PARTITION BY dp.PRICE_DATE, dp.market, ds.NAME, dp.PROD_ABBR_6, dp.BRAND_TYPE, dp.TCN_CODE) AS FILE_DT_TIME,
        DP.BRAND_TYPE,
        DP.MOVE_AMT,
        DP.EFFECTIVE_DT,
        CASE WHEN DP.CONFIRM_IND = 'Y' THEN DP.SUPPLIER ELSE NULL END AS Supplier,
        DP.TCN_CODE,
        DP.TERMINAL_CITY,
        DP.TERMINAL_OWNER_CODE,
        DP.PAYM_TERM,
        CAST(CASE WHEN DP.SUPPLIER = 'RA' THEN G_AGG.RA
                    WHEN DP.SUPPLIER = 'UA' THEN G_AGG.UA
                    WHEN DP.SUPPLIER = 'BA' THEN G_AGG.BA
                    WHEN DP.SUPPLIER = 'RH' THEN G_AGG.RH
                    WHEN DP.SUPPLIER = 'UH' THEN G_AGG.UH
                    WHEN DP.SUPPLIER = 'BH' THEN G_AGG.BH
                    WHEN DP.SUPPLIER = 'RL' THEN G_AGG.RL
                    WHEN DP.SUPPLIER = 'UL' THEN G_AGG.UL
                    WHEN DP.SUPPLIER = 'BL' THEN G_AGG.BL
                    ELSE DP.PRICE_GROSS END AS decimal(38,26)) AS Gross,
        CAST(DP.PRICE_NET AS decimal(38,26)) AS Net,
        DP.SUFFIX,
        DP.TERMINAL_CITY_NAME,
        DT.TERMINAL_STATE,
        DP.LATITUDE,
        DP.LONGITUDE,
        DT.TERMINAL_ADDRESS,
        DP.TERMINAL_ZIP,
        DP.PROD_NAME,
        DP.PROD_ABBR_6,
        DP.PROD_GROUP_NAME,
        DP.PRODUCT_CATEGORY,
        CASE WHEN DP.CONFIRM_IND IS NULL THEN DP.SUPPLIER ELSE 'Others' END AS Aggregate,
        DS.NAME AS Supplier_Name,
        CASE WHEN dp.SUPPLIER IN ('RA','BA','UA','RH','BH','UH','RL','BL','UL', 'TA', 'CA') AND (dp.PRICE_GROSS IS NULL AND dp.brand_type IS NULL) THEN dp.price_date_logic
                WHEN dp.SUPPLIER IN ('RA','BA','UA','RH','BH','UH','RL','BL','UL', 'TA', 'CA') AND (dp.PRICE_net IS NULL AND dp.brand_type IS NULL) THEN G_AGG.PDL1
        ELSE DP.PRICE_DATE_LOGIC END AS PRICE_DATE_LOGIC,
        DP.Prod_all_season
    FROM {{ref('Subquery')}} DP
    LEFT JOIN {{ref('G_AGG')}} G_AGG ON DP.MARKET = G_AGG.MARKET AND date(to_char(dp.PRICE_DATE),'YYYYMMDD') = G_AGG.PRICE_DATE AND DP.PROD_ABBR_6 = G_AGG.PROD_ABBR_6
    LEFT JOIN {{ref('DT')}} DT ON DP.TCN_CODE = DT.TCN
    LEFT JOIN {{source('DTN_HELPERS','DTN_SUPPLIERS')}} DS ON DS.ABBR = DP.SUPPLIER AND DS.BRAND_TYPE = DP.BRAND_TYPE
    WHERE ((DP.CONFIRM_IND IS NULL and DP.SUPPLIER IN ('RA','BA','UA','RH','BH','UH','RL','BL','UL', 'TA', 'CA')) OR DP.CONFIRM_IND = 'Y')