{{config(materialized = 'ephemeral')}}

SELECT DATEADD('DAY',-2,date(date)) PRICE_DATE, date(date) DATE, date(date) DATE2, 'Optional' AS holiday, 'Optional' AS holiday2 FROM {{ref('HOLIDAY')}}		
UNION
SELECT DATEADD('DAY',-1,date(date)) PRICE_DATE, date(date) DATE, date(date) DATE2, holiday, 'Real Holiday' as holiday2 FROM {{ref('HOLIDAY')}}		
UNION	
SELECT date(date) PRICE_DATE, date(date) DATE, date(date) DATE2, holiday, 'Real Holiday' as holiday2 FROM {{ref('HOLIDAY')}}
UNION
SELECT DATEADD('DAY',1,date(date)) PRICE_DATE, date(date) DATE, NULL AS DATE2, holiday, NULL AS holiday2 FROM {{ref('HOLIDAY')}}