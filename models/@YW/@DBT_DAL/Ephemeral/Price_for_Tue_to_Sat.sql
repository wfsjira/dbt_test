{{config(materialized = 'ephemeral')}}
SELECT PROD_ABBR_6, every_week, MARKET, 
    avg(price_gross) AS RA, 
    min(price_gross) AS RL, 
    max(price_gross) AS RH, 
    AVG(CASE WHEN brand_type = 'B' THEN PRICE_GROSS END) AS BA,
    AVG(CASE WHEN brand_type = 'U' THEN PRICE_GROSS END) AS UA,
    MAX(CASE WHEN brand_type = 'B' THEN PRICE_GROSS END) AS BH,
    MAX(CASE WHEN brand_type = 'U' THEN PRICE_GROSS END) AS UH,
    min(CASE WHEN brand_type = 'B' THEN PRICE_GROSS END) AS BL,
    min(CASE WHEN brand_type = 'U' THEN PRICE_GROSS END) AS UL,
    MAX(PRICE_DATE_LOGIC) AS PDL1,
    max(date) AS Holiday_Date,
    every_week AS PRICE_DATE,
    max(Holiday2) AS Holiday2
FROM {{ref('Subquery')}} SubQuery
WHERE Datename IN ('Tue', 'Wed', 'Thu', 'Fri', 'Sat')
    AND PROD_ABBR_6 IS NOT NULL
    AND every_week IS NOT NULL
    AND MARKET IS NOT NULL
    AND brand_type IS NOT NULL			 
GROUP BY PROD_ABBR_6, every_week, MARKET
HAVING Holiday_Date != DATEADD('DAY',-1,every_week)
    AND Holiday_Date != every_week
    OR holiday_date IS NULL