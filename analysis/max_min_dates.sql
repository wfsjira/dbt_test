    SELECT folder_name, client_number,
    min(date(reading_date_time)) AS min_date,
    max(date(reading_date_time)) AS max_date   
    FROM {{ref('W_NEM12_METER_READING_V')}}
    GROUP BY folder_name, client_number