--DTP fact fuel_flag test

select 
case when ap_inv_fuel_flag = dtp_fuel_flag 
then 0
else 1 end as fuel_flag_test
from (
with DTP as (select * from {{ ref('W_DTP_F')}}
        )

        select inv.invoice_id, decode(inv.fuel_flag, 1,'Y','N') as ap_inv_fuel_flag ,d.invoice_id,d.fuel_flag as dtp_fuel_flag
        from (
            select invoice_id,
            fuel_flag,
            row_number() over (partition by invoice_id order by fuel_flag desc) as row_number
            from (
                select invoice_id,
                decode(fuel_flag, 'Y',1,0) as fuel_flag
                from stg.w_ap_invoice_fs f
                where invoice_id in (select p.invoice_id from DTP as p order by invoice_id desc limit 100 )
            )
        ) as inv,
        DTP as d
        where row_number = 1
        and d.invoice_id = inv.invoice_id
)
where not (fuel_flag_test =0)
        